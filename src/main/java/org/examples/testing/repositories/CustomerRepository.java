package org.examples.testing.repositories;

import org.examples.testing.entities.Customer;
import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String>{
    
    @Query("SELECT c FROM Customer c WHERE c.id like ?1")
    public List<Customer> searchLikeId(String id);
}
