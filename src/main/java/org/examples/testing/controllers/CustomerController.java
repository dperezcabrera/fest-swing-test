package org.examples.testing.controllers;

import javax.swing.JTable;

public interface CustomerController {

    public void searchCustomer(String id, JTable result);
    public void addCustomer(String id, String name);
}
