package org.examples.testing.controllers;

import org.examples.testing.entities.Customer;
import org.examples.testing.services.CustomerService;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class CustomerControllerImpl implements CustomerController {
    
    @Autowired
    CustomerService customerService;
    
    @Override
    public void searchCustomer(String id, JTable result){
        List<Customer> customers = customerService.customerSearch(id);
        DefaultTableModel model = (DefaultTableModel) result.getModel();
        while (model.getRowCount() > 0){
            model.removeRow(0);
        }
        for (Customer customer : customers) {
            model.addRow(new Object[]{customer.getId(),customer.getName()});
        }
    }

    @Override
    public void addCustomer(String id, String name) {
        customerService.addCustomer(id, name);
    }
}
