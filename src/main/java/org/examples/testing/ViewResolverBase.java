package org.examples.testing;

import java.awt.Window;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ViewResolverBase implements ViewResolver {

    public static final ViewResolver INSTANCE = new ViewResolverBase();
    private ApplicationContext applicationContext;

    private ViewResolverBase() {
        this.applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
    }

    @Override
    public <T extends Window> T getView(Class<T> type) {
        return applicationContext.getBean(type);
    }
    
    @Override
    public <T> T getComponent(Class<T> type) {
        return applicationContext.getBean(type);
    }
}
