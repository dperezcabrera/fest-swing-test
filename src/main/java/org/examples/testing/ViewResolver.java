package org.examples.testing;

import java.awt.Window;

public interface ViewResolver {

    public <T extends Window> T getView(Class<T> type);

    public <T> T getComponent(Class<T> type);
}
