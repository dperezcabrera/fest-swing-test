package org.examples.testing;

import org.examples.testing.services.CustomerService;
import org.examples.testing.views.ViewForm;

public class Main {

    public static void main(String args[]) {
        CustomerService service = ViewResolverBase.INSTANCE.getComponent(CustomerService.class);
        service.addCustomer("000001", "Alice");
        service.addCustomer("000002", "Bob");
        service.addCustomer("000003", "Peter");
        service.addCustomer("000010", "John");
        ViewResolverBase.INSTANCE.getView(ViewForm.class).setVisible(true);
    }
}
