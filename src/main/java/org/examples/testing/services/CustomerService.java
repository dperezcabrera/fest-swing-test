package org.examples.testing.services;

import org.examples.testing.entities.Customer;
import java.util.List;

public interface CustomerService {

    public List<Customer> customerSearch(String id);
    public Customer addCustomer(String id, String name);
}
