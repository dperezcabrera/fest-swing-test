package org.examples.testing.services;

import java.util.List;
import org.examples.testing.entities.Customer;
import org.examples.testing.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public List<Customer> customerSearch(String id) {
        return customerRepository.searchLikeId("%"+id+"%");
    }

    @Override
    @Transactional
    public Customer addCustomer(String id, String name) {
        return customerRepository.save(new Customer(id, name));
    }
}
